defmodule VectorexTest do
  use ExUnit.Case
  doctest Vectorex
  alias Vectorex

  defmacrop macro_expand(map) do
    quote do
      unquote(map).column_value
    end
  end

  describe "test to_sql for to_tsquery" do
    test "with macro" do
      map = %{column_value: "mycolumn"}

      result =
        Vectorex.new(:to_tsquery, macro_expand(map))
        |> Vectorex.to_sql()

      assert result == "mycolumn"
    end

    test "using ts_and and list of parameters" do
      result =
        Vectorex.new(:to_tsquery, "elixir")
        |> Vectorex.ts_and(["erlang", "ocaml"])
        |> Vectorex.to_sql()

      assert result == "elixir & erlang & ocaml"
    end

    test "using ts_and and list of invalid parameters is ignored" do
      result =
        Vectorex.new(:to_tsquery, "elixir")
        |> Vectorex.ts_and([:invalid, :parameter])
        |> Vectorex.to_sql()

      assert result == "elixir"
    end

    test "using ts_and and list of parameters in subquery and main query" do
      inner =
        Vectorex.Subquery.new("ocaml")
        |> Vectorex.Subquery.ts_or(["scala", "haskell"])

      result =
        Vectorex.new(:to_tsquery, "elixir")
        |> Vectorex.ts_or(["erlang", "java"])
        |> Vectorex.ts_and(inner)
        |> Vectorex.to_sql()

      assert result == "elixir | erlang | java & (ocaml | scala | haskell)"
    end

    test "using ts_or and list of parameters" do
      result =
        Vectorex.new(:to_tsquery, "elixir")
        |> Vectorex.ts_or(["erlang", "ocaml"])
        |> Vectorex.to_sql()

      assert result == "elixir | erlang | ocaml"
    end

    test "using ts_not and list of parameters" do
      result =
        Vectorex.new(:to_tsquery, "elixir")
        |> Vectorex.ts_not(["erlang", "ocaml"])
        |> Vectorex.to_sql()

      assert result == "elixir & !erlang & !ocaml"
    end

    test "using no cases" do
      result =
        Vectorex.new(:to_tsquery, "elixir")
        |> Vectorex.to_sql()

      assert result == "elixir"
    end

    test "using one and case" do
      result =
        Vectorex.new(:to_tsquery, "elixir")
        |> Vectorex.ts_and("ocaml")
        |> Vectorex.to_sql()

      assert result == "elixir & ocaml"
    end

    test "using one and case and one invalid parameter" do
      result =
        Vectorex.new(:to_tsquery, "elixir")
        |> Vectorex.ts_and({:invalid, [:parameter, :to_query]})
        |> Vectorex.to_sql()

      assert result == "elixir"
    end

    test "using one or case" do
      result =
        Vectorex.new(:to_tsquery, "elixir")
        |> Vectorex.ts_or("ocaml")
        |> Vectorex.to_sql()

      assert result == "elixir | ocaml"
    end

    test "using one not case" do
      result =
        Vectorex.new(:to_tsquery, "elixir")
        |> Vectorex.ts_not("ocaml")
        |> Vectorex.to_sql()

      assert result == "elixir & !ocaml"
    end

    test "using followed by" do
      result =
        Vectorex.new(:to_tsquery, "elixir")
        |> Vectorex.ts_followed_by("ocaml")
        |> Vectorex.to_sql()

      assert result == "elixir <-> ocaml"
    end

    test "using not and quotes" do
      result =
        Vectorex.new(:to_tsquery, ~s(''supernovae stars''))
        |> Vectorex.ts_not("ocaml")
        |> Vectorex.to_sql()

      assert result == "''supernovae stars'' & !ocaml"
    end

    test "using one or and one and case" do
      result =
        Vectorex.new(:to_tsquery, "elixir")
        |> Vectorex.ts_or("ocaml")
        |> Vectorex.ts_and("scala")
        |> Vectorex.to_sql()

      assert result == "elixir | ocaml & scala"
    end

    test "using one or one and and one not case" do
      result =
        Vectorex.new(:to_tsquery, "elixir")
        |> Vectorex.ts_or("ocaml")
        |> Vectorex.ts_and("scala")
        |> Vectorex.ts_not("php")
        |> Vectorex.to_sql()

      assert result == "elixir | ocaml & scala & !php"
    end

    test "using followed in subquery" do
      inner =
        Vectorex.Subquery.new("ocaml")
        |> Vectorex.Subquery.ts_followed_by("scala")

      result =
        Vectorex.new(:to_tsquery, "elixir")
        |> Vectorex.ts_and(inner)
        |> Vectorex.to_sql()

      assert result == "elixir & (ocaml <-> scala)"
    end

    test "using one or and one and case nested" do
      inner =
        Vectorex.Subquery.new("ocaml")
        |> Vectorex.Subquery.ts_and("scala")

      result =
        Vectorex.new(:to_tsquery, "elixir")
        |> Vectorex.ts_or(inner)
        |> Vectorex.to_sql()

      assert result == "elixir | (ocaml & scala)"
    end

    test "subquery using a list" do
      inner =
        Vectorex.Subquery.new("ocaml")
        |> Vectorex.Subquery.ts_and(["scala", "haskell"])

      result =
        Vectorex.new(:to_tsquery, "elixir")
        |> Vectorex.ts_or(inner)
        |> Vectorex.to_sql()

      assert result == "elixir | (ocaml & scala & haskell)"
    end

    test "using list of subqueries" do
      inner =
        Vectorex.Subquery.new("ocaml")
        |> Vectorex.Subquery.ts_or(["scala", "haskell"])

      inner2 =
        Vectorex.Subquery.new("java")
        |> Vectorex.Subquery.ts_or("c#")

      result =
        Vectorex.new(:to_tsquery, "elixir")
        |> Vectorex.ts_and([inner, inner2])
        |> Vectorex.to_sql()

      assert result == "elixir & (ocaml | scala | haskell) & (java | c#)"
    end

    test "using 2 nested or" do
      inner =
        Vectorex.Subquery.new("ocaml")
        |> Vectorex.Subquery.ts_and("scala")

      inner2 =
        Vectorex.Subquery.new("zig")
        |> Vectorex.Subquery.ts_or("nim")

      result =
        Vectorex.new(:to_tsquery, "elixir")
        |> Vectorex.ts_or(inner)
        |> Vectorex.ts_or(inner2)
        |> Vectorex.to_sql()

      assert result == "elixir | (ocaml & scala) | (zig | nim)"
    end

    test "using nested in nested" do
      inner =
        Vectorex.Subquery.new("ocaml")
        |> Vectorex.Subquery.ts_and("scala")

      inner2 =
        Vectorex.Subquery.new("zig")
        |> Vectorex.Subquery.ts_or("nim")
        |> Vectorex.Subquery.ts_or(inner)

      result =
        Vectorex.new(:to_tsquery, "elixir")
        |> Vectorex.ts_or(inner2)
        |> Vectorex.to_sql()

      assert result == "elixir | (zig | nim | (ocaml & scala))"
    end
  end

  describe "to_sql for websearch_to_tsquery" do
    test "using or" do
      result =
        Vectorex.new(:websearch_to_tsquery, ~s("sad cat"))
        |> Vectorex.ts_or(~s("fat rat"))
        |> Vectorex.to_sql()

      assert result == ~s"sad cat" or "fat rat"
    end

    test "using list of or" do
      result =
        Vectorex.new(:websearch_to_tsquery, ~s("sad cat"))
        |> Vectorex.ts_or([~s("fat rat"), ~s("ugly rat")])
        |> Vectorex.to_sql()

      assert result == ~s("sad cat" or "fat rat" or "ugly rat")
    end

    test "using not" do
      result =
        Vectorex.new(:websearch_to_tsquery, ~s("supernovae stars"))
        |> Vectorex.ts_not("crab")
        |> Vectorex.to_sql()

      assert result == ~s"\"supernovae stars\" -crab"
    end

    test "using not list" do
      result =
        Vectorex.new(:websearch_to_tsquery, ~s("supernovae stars"))
        |> Vectorex.ts_not(["crab", "rust"])
        |> Vectorex.to_sql()

      assert result == ~s"\"supernovae stars\" -crab -rust"
    end

    test "using not in quotes" do
      result =
        Vectorex.new(:websearch_to_tsquery, "signal")
        |> Vectorex.ts_not(~s("segmentation fault"))
        |> Vectorex.to_sql()

      assert result == ~s(signal -"segmentation fault")
    end

    test "using list of not in quotes" do
      result =
        Vectorex.new(:websearch_to_tsquery, "signal")
        |> Vectorex.ts_not([~s("segmentation fault"), ~s("core dumped")])
        |> Vectorex.to_sql()

      assert result == ~s(signal -"segmentation fault" -"core dumped")
    end
  end

  describe "to_sql for plainto_tsquery" do
    test "using and adds a space between terms" do
      result =
        Vectorex.new(:plainto_tsquery, "term1")
        |> Vectorex.ts_and("term2")
        |> Vectorex.to_sql()

      assert result == "term1 term2"
    end

    test "using or ignores the terms" do
      result =
        Vectorex.new(:plainto_tsquery, "term1")
        |> Vectorex.ts_or("term2")
        |> Vectorex.to_sql()

      assert result == "term1"
    end

    test "using and with a term that contains a space creates 3 terms" do
      result =
        Vectorex.new(:plainto_tsquery, "term1")
        |> Vectorex.ts_and("term 2")
        |> Vectorex.to_sql()

      assert result == "term1 term 2"
    end
  end

  describe "to_sql for phraseto_tsquery" do
    test "using followed by adds a space" do
      result =
        Vectorex.new(:phraseto_tsquery, "term1")
        |> Vectorex.ts_followed_by("term2")
        |> Vectorex.to_sql()

      assert result == "term1 term2"
    end

    test "using and is ignored" do
      result =
        Vectorex.new(:phraseto_tsquery, "term1")
        |> Vectorex.ts_and("term2")
        |> Vectorex.to_sql()

      assert result == "term1"
    end

    test "using or is ignored" do
      result =
        Vectorex.new(:phraseto_tsquery, "term1")
        |> Vectorex.ts_or("term2")
        |> Vectorex.to_sql()

      assert result == "term1"
    end

    test "using not is ignored" do
      result =
        Vectorex.new(:phraseto_tsquery, "term1")
        |> Vectorex.ts_not("term2")
        |> Vectorex.to_sql()

      assert result == "term1"
    end
  end
end

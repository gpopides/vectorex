defmodule Vectorex.MixProject do
  use Mix.Project

  def project do
    [
      app: :vectorex,
      version: "0.1.0",
      elixir: "~> 1.16",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      source_url: "https://gitlab.com/gpopides/vectorex",
      docs: [
        main: "Vectorex",
        extras: ["README.md"]
      ],
      package: package(),
      description: description()
    ]
  end

  def application do
    []
  end

  defp deps do
    [
      {:ex_doc, "~> 0.31.0"}
    ]
  end

  defp description() do
    "Vectorex is a builder for [full text search controls](https://www.postgresql.org/docs/current/textsearch-controls.html) for Postgres.
      Instead of concatenating strings to build a `ts_vector`, Vectorex provides a minimal API to do it fluently."
  end

  defp package() do
    [
      licenses: ["Apache-2.0"],
      links: %{"Gitlab" => "https://gitlab.com/gpopides/vectorex"}
    ]
  end
end

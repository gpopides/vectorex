# Vectorex

Vectorex is a builder for [full text search controls](https://www.postgresql.org/docs/current/textsearch-controls.html) for Postgres.
Instead of concatenating strings to build a `ts_vector`, Vectorex provides a minimal API to do it fluently.

After building the query, you can use `to_sql` to convert it to the string that the database expects and parses.

### Examples

Starting with the simplest case where we want to create q `ts_query` which will match the word `elixir`

```elixir
vectorex = Vectorex.new(:to_tsquery, "elixir")
```

We can add a case where we want to also match the word *ocaml*

```elixir
vectorex = Vectorex.ts_and(vectorex, "ocaml")
```

We also want to match *scala* and not only *ocaml*

```elixir
vectorex = Vectorex.ts_and(vectorex, "ocaml") |> Vectorex.ts_and("ocaml")
```

Maybe we want to match scala and ocaml or *haskell*

```elixir
vectorex = Vectorex.ts_and(vectorex, "ocaml") 
|> Vectorex.ts_and("ocaml")
|> Vectorex.ts_or("haskell")
```

We can use subqueries to group controls together. Lets say we want to match (scala and ocaml) or *haskell*
```elixir
subquery = Vectorex.Subquery.new("ocaml")
|> Vectorex.Subquery.ts_and("scala")

vectorex = Vectorex.ts_and(vectorex, "ocaml") 
|> Vectorex.ts_and(subquery)
|> Vectorex.ts_or("haskell")
```


### Converting to a string 

After we build the query, we can use `to_sql` to convert it to a string and pass it to Ecto for example for execution

```elixir
    vectorex = Vectorex.new(:to_tsquery, "elixir")

    from e in Event,
        where: fragment("textsearchable_index_col @@ to_tsquery(?)", ^Vectorex.to_sql(vectorex))
```

You can find more examples in [documentation](https://hexdocs.pm/vectorex/Vectorex.html)

## Installation

```elixir
def deps do
  [
    {:vectorex, "~> 0.1.0"}
  ]
end
```
